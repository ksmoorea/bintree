//BinTreeTest.java
//Kristen Swanner
//COSC 2336.301
//September 26, 2019

package bintree;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class BinTreeTest {
	
	@Test
	void testConstructor() {
		 BinTree test_article = new BinTree();
		 assertEquals("O", test_article.getBloodType());
		 assertEquals("+", test_article.getRHFactor());
	}
	
	@Test
	void testOverloadedConstructor() {
		 BinTree test_article = new BinTree("O", "-");
		 assertEquals("O", test_article.getBloodType());
		 assertEquals("-", test_article.getRHFactor());
	}

	@Test
	void testBadBloodData() {
		 BinTree test_article = new BinTree("X", "++");
		 assertEquals("INVALID", test_article.getBloodType());
		 assertEquals("INVALID", test_article.getRHFactor());
	
	}
}
