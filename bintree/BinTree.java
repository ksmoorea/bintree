//BinTree.java
//Kristen Swanner
//COSC 2336.301
//September 26, 2019

package bintree;

public class BinTree {
	
	private String BloodType;
	private String RHFactor;
	
	public BinTree() {
		setBloodType("O");
		setRHFactor("+");
	}
	
	public BinTree(String blood_type, String rh_factor) {
		setBloodType(blood_type);
		setRHFactor(rh_factor);
	}

	public String getBloodType() {
		return BloodType;
	}

	public void setBloodType(String blood_type) {
		if(blood_type == "AB" || blood_type == "O" || blood_type == "A" || blood_type == "B") {
			BloodType = blood_type;	
		}else {
			BloodType = "INVALID";		
		}
	}

	public String getRHFactor() {
		return RHFactor;
	}

	public void setRHFactor(String rh_factor) {
		if(rh_factor == "+" || rh_factor == "-" ) {
			RHFactor = rh_factor;	
		}else {
			RHFactor = "INVALID";		
		}
	}

}
